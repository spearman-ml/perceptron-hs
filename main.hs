#!/usr/bin/env runhaskell

-- | Train perceptron and run multilayer XOR perceptron network

--{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE DuplicateRecordFields #-}

import Prelude hiding ((<>))
import Data.Bool     (bool)
import Text.Printf   (printf)

import qualified System.Random         as Random
import Numeric.LinearAlgebra ((!), (#>), (|>), (><), (<.>), Vector, vector)
import qualified Numeric.LinearAlgebra as LinearAlgebra

import Graphics.Rendering.Chart.Easy ((.=))
import qualified Graphics.Rendering.Chart.Easy             as Chart
import qualified Graphics.Rendering.Chart.Backend.Diagrams as Diagrams

class Neuron a where
  output :: a -> Vector Double -> Double
  learn  :: a -> (Vector Double, Double) -> a

data Perceptron = Perceptron {
  -- includes bias as the first element
  weights :: Vector Double
} deriving (Show)

instance Neuron Perceptron where
  output :: Perceptron -> Vector Double -> Double
  output perceptron input = if
      (LinearAlgebra.vjoin
        [vector [1.0], input] <.> weights (perceptron :: Perceptron)
      ) > 0.0
    then
      1.0
    else
      0.0
  learn :: Perceptron -> (Vector Double, Double) -> Perceptron
  learn perceptron (sample_input, sample_output) = Perceptron new_weights where
    predicted   = output perceptron sample_input
    new_weights = (weights (perceptron :: Perceptron))
      - LinearAlgebra.scale (predicted - sample_output)
        (LinearAlgebra.vjoin [vector [1.0], sample_input])

train :: Neuron a => Int -> [(Vector Double, Double)] -> a -> a
train 0 _ neuron = neuron
train n dataset neuron = train (n-1) dataset trained where
  trained = foldl learn neuron dataset

-- | Line endpoints for plotting.
-- | Note: doesn't handle models where w2 is zero (vertical line).
decision_boundary :: Perceptron -> [(Double, Double)]
decision_boundary perceptron = [(min!0, min!1), (max!0, max!1)] where
  min         = y_intercept - 100.0 * slope
  max         = y_intercept + 100.0 * slope
  ws          = weights (perceptron :: Perceptron)
  y_intercept = vector [0.0, (-(ws!0))/(ws!2)]
  slope       = LinearAlgebra.normalize
    $ ((2><2) [0.0, -1.0, 1.0, 0.0]) #> (LinearAlgebra.subVector 1 2 ws)

multilayer_perceptron :: (Vector Double, Perceptron, Perceptron, Perceptron)
  -> Double
multilayer_perceptron (input, a, b, c)
  = output c $ vector [output a input, output b input]

main :: IO ()
main = do
  putStrLn "perceptron main..."

  stdgen1 <- Random.newStdGen
  let true_weights    = vector (take 3 $ Random.randomRs ((-10), 10) stdgen1)
        :: Vector Double
  let true_perceptron = Perceptron true_weights
  putStrLn $ "true weights: " ++ show true_perceptron

  stdgen2 <- Random.newStdGen
  stdgen3 <- Random.newStdGen
  let sample_size = 100
  let sample_regressors = zipWith (\x y -> vector [x, y])
        (take sample_size $ Random.randomRs ((-10.0), 10.0) stdgen2)
        (take sample_size $ Random.randomRs ((-10.0), 10.0) stdgen3)
        :: [Vector Double]
  let training_set = zip sample_regressors $
        map (\p -> output true_perceptron p) sample_regressors
        :: [(Vector Double, Double)]
  --putStrLn $ "training set: " ++ show training_set

  let initial_weights    = 3 |> [0.0, 0.0, 0.0]
  let initial_perceptron = Perceptron initial_weights
  putStrLn $ "initial weights: " ++ show initial_perceptron
  let trained_perceptron = train 100 training_set initial_perceptron
  putStrLn $ "trained weights: " ++ show trained_perceptron

  let inset  = (map (\(p, b) -> (p!0, p!1)) . filter (\(_, b) -> b == 1.0))
        training_set
  let outset = (map (\(p, b) -> (p!0, p!1)) . filter (\(_, b) -> b == 0.0))
        training_set
  let true_boundary    = decision_boundary true_perceptron
  let trained_boundary = decision_boundary trained_perceptron

  Diagrams.toFile Chart.def "plot.svg" $ do
    Chart.layout_title  .= "training set"
    Chart.layout_x_axis . Chart.laxis_override .=
      (const $ Chart.autoAxis [-10.0, 10.0])
    Chart.layout_y_axis . Chart.laxis_override .=
      (const $ Chart.autoAxis [-10.0, 10.0])
    Chart.plot $ Chart.liftEC $ do
      Chart.plot_points_title  .= "in"
      Chart.plot_points_values .= inset
      Chart.plot_points_style  . Chart.point_color .= Chart.opaque Chart.magenta
    Chart.plot $ Chart.liftEC $ do
      Chart.plot_points_title  .= "out"
      Chart.plot_points_values .= outset
      Chart.plot_points_style  . Chart.point_color .= Chart.opaque Chart.cyan
    Chart.plot $ Chart.liftEC $ do
      Chart.plot_lines_title   .= "decision boundary"
      Chart.plot_lines_values  .= [true_boundary]
      Chart.plot_lines_style   . Chart.line_color  .= Chart.opaque Chart.green
    Chart.plot $ Chart.liftEC $ do
      Chart.plot_lines_title   .= "trained boundary"
      Chart.plot_lines_values  .= [trained_boundary]
      Chart.plot_lines_style   . Chart.line_color  .= Chart.opaque Chart.red

  putStrLn "CONST ZERO"
  let por = Perceptron $ vector [0.0, 0.0, 0.0]
  putStrLn $ "(0,0): " ++ (show $ output por $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output por $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output por $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output por $ vector [1.0, 1.0])

  putStrLn "CONST ONE"
  let por = Perceptron $ vector [1.0, 0.0, 0.0]
  putStrLn $ "(0,0): " ++ (show $ output por $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output por $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output por $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output por $ vector [1.0, 1.0])

  putStrLn "OR"
  let por = Perceptron $ vector [0.0, 1.0, 1.0]
  putStrLn $ "(0,0): " ++ (show $ output por $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output por $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output por $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output por $ vector [1.0, 1.0])

  putStrLn "AND"
  let pand = Perceptron $ vector [-1.0, 1.0, 1.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "NAND"
  let pand = Perceptron $ vector [2.0, -1.0, -1.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "NOR"
  let pand = Perceptron $ vector [1.0, -1.0, -1.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "X"
  let pand = Perceptron $ vector [0.0, 1.0, 0.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "Y"
  let pand = Perceptron $ vector [0.0, 0.0, 1.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "NOT X AND Y"
  let pand = Perceptron $ vector [0.0, -1.0, 1.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "NOT Y AND X"
  let pand = Perceptron $ vector [0.0, 1.0, -1.0]
  putStrLn $ "(0,0): " ++ (show $ output pand $ vector [0.0, 0.0])
  putStrLn $ "(0,1): " ++ (show $ output pand $ vector [0.0, 1.0])
  putStrLn $ "(1,0): " ++ (show $ output pand $ vector [1.0, 0.0])
  putStrLn $ "(1,1): " ++ (show $ output pand $ vector [1.0, 1.0])

  putStrLn "XOR"
  let a = Perceptron $ vector [ 0.0,  1.0,  1.0]  -- OR
  let b = Perceptron $ vector [ 2.0, -1.0, -1.0]  -- NAND
  let c = Perceptron $ vector [-1.0,  1.0,  1.0]  -- AND
  putStrLn $ "(0,0): "
    ++ (show $ multilayer_perceptron (vector [0.0, 0.0], a, b, c))
  putStrLn $ "(0,1): "
    ++ (show $ multilayer_perceptron (vector [0.0, 1.0], a, b, c))
  putStrLn $ "(1,0): "
    ++ (show $ multilayer_perceptron (vector [1.0, 0.0], a, b, c))
  putStrLn $ "(1,1): "
    ++ (show $ multilayer_perceptron (vector [1.0, 1.0], a, b, c))

  putStrLn "...perceptron main"
